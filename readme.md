💡💡💡 Using flux to implement a delivery pipeline 💡💡💡


Create "prod" and "stage" clusters in civo

🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️🚑️

```
kind create cluster --name=dev --config=kind-config.yml

kubectl apply -f nginx-ingress-deploy.yaml

kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s

source .gitlab-token.sh

flux bootstrap gitlab \
  --components-extra=image-reflector-controller,image-automation-controller \
  --owner=nautik1 \
  --repository=kind-test-cluster \
  --branch=master \
  --path=clusters/01-dev \
  --token-auth \
  --personal
```

🔥🔥🔥 Dev cluster is now ready 🔥🔥🔥

Download stage and prod creds

```
export KUBECONFIG=$PWD/civo-creds-prod.yml:$PWD/civo-creds-stage.yml:$HOME/.kube/config

kubectl config use-context stage

flux bootstrap gitlab \
  --components-extra=image-reflector-controller,image-automation-controller \
  --owner=nautik1 \
  --repository=kind-test-cluster \
  --branch=master \
  --path=clusters/02-stage \
  --token-auth \
  --personal
```

🔖🔖🔖 Stage cluster is ready 🔖🔖🔖

```
kubectl config use-context prod

flux bootstrap gitlab \
  --components-extra=image-reflector-controller,image-automation-controller \
  --owner=nautik1 \
  --repository=kind-test-cluster \
  --branch=master \
  --path=clusters/03-prod \
  --token-auth \
  --personal
```

✨✨✨ Prod cluster is ready ✨✨✨
