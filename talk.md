
- Demo:
    - Create kubernetes cluster for dev (stage and production are already up)
    - Setup GitOps tooling: Deploy flux and watch it deploy my application
    - Ship a new article in my blog!
- Commentaires
- Discussion / Questions

Key points
----------

- Flux makes sure I can be confident that what is in the git repo is what is deployed in production
    - I can act on all environments without accessing them myself
    - Pull-model not push-model, clusters need acces to git & registry but no need for the other way around
- The *only* critical system is: my 2 git repositories.
    - Everything else can be re-deployed within a few minutes, images can be rebuilt (even history)...
- Docker registry gets used a lot
    - Deduplication of layers is nice for storage
    - Auto-cleanup is usually easy to setup
    - HA can be made easy (e.g. object storage)
- Every part of the process (I can think of) can be reproduced locally
    - I can even use prod infrastructure manifests in my local cluster (to some extent)
- Requesting new infrastructure (app deployment, load balancer, storage volume...) is now "getting a Merge Request approved"
- Replaces existing stack
- All tooling is upstream: I developped nothing else than my application
